import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import VideoItem from './component/videoitem.js'
import data from './data.json'

export default class App extends Component {
  render() {
    return (
    <View View style = {styles.container} >
      <View style={styles.navBar}>
        <Image source={require('./images/logo.png')} style={{width: 98, height: 22}} />
        <View style={styles.RightNav}>
          <TouchableOpacity><Icon style={styles.NavItems} name="search" size={25}/></TouchableOpacity >
          <TouchableOpacity><Icon style={styles.NavItems}  name="account-circle" size={25} /></TouchableOpacity>
        </View>
      </View>
      <View style={styles.body}>
        <FlatList data={data.items} 
        renderItem={(video)=><VideoItem video={video.item}/>} 
        ItemSeparatorComponent={()=><View style={{height: 0.5, backgroundColor: "#cccc"}} />}
        />
      </View>
      <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name="home" size={25}/>
            <Text style={styles.TabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name="whatshot" size={25} />
            <Text style={styles.TabTitle}>Trending</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name="subscriptions" size={25} />
            <Text style={styles.TabTitle}>Subscriptions</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name="folder" size={25} />
            <Text style={styles.TabTitle}>Library</Text>
          </TouchableOpacity>
      </View>
    </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: getStatusBarHeight(),
  },
  navBar: {
    height: 55,
    backgroundColor: "white",
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  RightNav: {
    flexDirection: 'row'
  },
  NavItems: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: "white",
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  tabItems: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  TabTitle: {
    fontSize: 11
  }
})