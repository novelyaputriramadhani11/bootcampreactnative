console.log('---------- 1 ----------')
console.log('LOOPING PERTAMA')
var i = 2
while(i < 21){
    console.log(i+' - I love coding')
    i+=2
}
console.log('LOOPIING KEDUA')
var i = 20
while(i>1){
    console.log(i+' - I will become a mobile developer')
    i-=2
}
console.log('---------- 2 ----------')
for(i = 1 ; i<=20 ; i++){
    if(i%2==0){
        console.log(i +' - Berkualitas')
    }else if(i%3==0 && i%2==1){
        console.log(i+' - I Love Coding')
    }else{
        console.log(i+' - Santai')
    }
}
console.log('---------- 3 ----------')
var i = 1
var j = 1
var p = 8
var l = 4
var pagar = ' '
while(j<=l){
    while(i<=p){
        pagar += '#'
        i++
    }
    console.log(pagar)
    pagar=' '
    i = 1
    j++
}
console.log('---------- 4 ----------')
var i = 1
var j = 1
var a = 7
var t = 7
var pagar = ''
for(i=1;i<=t;i++){
    for(j=1;j<=i;j++){
        pagar +='#'
    }
    console.log(pagar)
    pagar=''
}
console.log('---------- 5 ----------')
i = 1
j = 1
var p = 8
var l = 8
var papan =''
for(j = 1;j<=l;j++){
    if(j%2==1){
        for(i=1;i<=p;i++){
            if(i%2==1){
                papan+=' '
            }else{
                papan+='#'
            }
        }
    }else{
        for(i=1;i<=p;i++){
            if(i%2==1){
                papan+='#'
            }else{
                papan+=' '
            }
        }
    }
    console.log(papan)
    papan=''
}