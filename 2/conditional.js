console.log('---------- if-else ----------')
var nama = 'Junaedi'
var peran = 'Werewolf'
if (nama == ' '){
    console.log('Nama harus diisi!')
}
else if (nama && peran == ' '){
    console.log('Halo ' + nama + '! Pilih Peranmu untuk memulai game.')
}
else if (nama == 'Jane' && peran == 'Penyihir'){
    console.log('Selamat datang di Dunia Werewolf, Jane \nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!')
}
else if (nama == 'Jenita' && peran == 'Guard'){
    console.log('Selamat datang di Dunia Werewolf, Jenita \nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.')
}
else if (nama == 'Junaedi' && peran == 'Werewolf'){
    console.log('Selamat datang di Dunia Werewolf, Junaedi \nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!')
}
console.log(' ')
console.log('---------- Switch Case ----------')
var hari = 11
var bulan = 11
var tahun = 2001
var namabulan
if (hari >= 1 && hari <= 31){
    if (bulan >= 1 && bulan <= 12){
        if (tahun >= 1900 && tahun <= 2200){
            switch(bulan){
                case 1:
                    namabulan = 'Januari'
                    break
                case 2:
                    namabulan = 'Februari'
                    break
                case 3:
                    namabulan = 'Maret'
                    break
                case 4:
                    namabulan = 'April'
                    break
                case 5:
                    namabulan = 'Mei'
                    break
                case 6:
                    namabulan = 'Juni'
                    break
                case 7:
                    namabulan = 'Juli'
                    break
                case 8:
                    namabulan = 'Agustus'
                    break
                case 9:
                    namabulan = 'September'
                case 10:
                    namabulan = 'Oktober'
                    break
                case 11:
                    namabulan = 'November'
                    break
                case 12:
                    namabulan = 'Desember'
                    break
                default:
                    break
            }
            console.log(hari + ' ' + namabulan + ' ' + tahun)
        }else{
            console.log('Masukkan tahun antara 1900-2200')
        }
    } else{
        console.log('Masukkan bulan antara 1-12')
      }
    }else{
        console.log('Masukkan hari antara 1-31')
}
