console.log('---------- 1 ----------')
function arrayToObject(array){
    if(array.length <= 0){
        return console.log('')
    }
    for(var i=0; i < array.length; i++){
        var BirthYear = array[i][3]
        var now = new Date().getFullYear()
        var object = {}
        var age1
        if(BirthYear && now - BirthYear > 0){
            age1 = now - BirthYear
        }else{
            age1 = 'Invalid Birth Year'
        }
        object.firstName = array[i][0]
        object.lastName = array[i][1]
        object.gender = array[i][2]
        object.age = age1
        var output = (i+1) + '. ' +object.firstName+' '+object.lastName+ ':'
        console.log(output)
        console.log(object)
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
console.log('*******')
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
console.log('---------- 2 ----------')
function shoppingTime(memberId, money){
    if(!memberId){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }else if(money < 50000){
        return 'Mohon maaf, uang tidak cukup'
    }else{
        var object = {}
        var change = money
        var purchase = []
        var sepatu = 'Sepatu Stacattu'
        var baju1 = 'Baju Zoro'
        var baju2 = 'Baju H&N'
        var sweater = 'Sweater Uniklooh'
        var casing = 'Casing Handphone'
        var check = 0
        for(var i=0; change >= 50000 && check==0;i++){
            if(change >= 1500000){
                purchase.push(sepatu)
                change -= 1500000
            }else if(change >= 500000){
                purchase.push(baju1)
                change -= 500000
            }else if(change >= 250000){
                purchase.push(baju2)
                change -= 250000
            }else if(change >= 175000){
                purchase.push(sweater)
                change -= 175000
            }else if(change>=50000){
                for(var j=0; j<= purchase.length-1; j++){
                    if(purchase[j]==casing){
                        check+=1
                    }
                }if(check==0){
                    purchase.push(casing)
                    change -= 50000
                }else{
                purchase.push(casing)
                change -= 50000
                }
            }
        }
        object.memberId = memberId
        object.money = money
        object.listPurchased = purchase
        object.changeMoney = change
        return object
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log('******')
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log('******')
console.log(shoppingTime('', 2475000))
console.log('******')
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log('******')
console.log(shoppingTime())
console.log('---------- 3 ----------')
function naikAngkot(arrPenumpang){
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var arrayoutput = []
    if(arrPenumpang.length <= 0){
        return []
    }
    for(var i=0;i<arrPenumpang.length;i++){
        var output = {}
        var dari = arrPenumpang[i][1]
        var tujuan = arrPenumpang[i][2]
        var index1
        var index2
    for(var j=0; j<rute.length;j++){
        if(rute[j]== dari){
            index1 = j
        }else if(rute[j]==tujuan){
            index2 = j
        }
    }
    var bayar = (index2 - index1)* 2000
    output.penumpang = arrPenumpang[i][0]
    output.naikDari = dari
    output.tujuan = tujuan
    output.bayar = bayar
    arrayoutput.push(output)
    }
    return arrayoutput
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))