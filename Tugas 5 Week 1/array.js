console.log('---------- 1 ----------')
function range(startNum, finishNum){
    var rangearray = []
    if(startNum < finishNum){
        var rangelength = finishNum - startNum + 1
        for(var i = 0;i < rangelength; i++){
            rangearray.push(startNum + i)
        }
    }else if(startNum > finishNum){
        var rangelength = startNum - finishNum + 1
        for(var i = 0;i < rangelength; i++){
            rangearray.push(startNum - i)
        }
    }else if(!startNum || !finishNum){
        return -1
    }
    return rangearray
}
console.log('range(1,10):' , range(1,10))
console.log('range(1):' , range(1))
console.log('range(11,18):' , range(11,18))
console.log('range(54,50):' , range(54,50))
console.log('range():' , range())
console.log('---------- 2 ----------')
function rangeWithStep(startNum, finishNum, step){
    var rangearray = []
    if(startNum < finishNum){
        var currentNum = startNum
        for(var i = 0;currentNum <= finishNum; i++){
            rangearray.push(currentNum)
            currentNum += step
        }
    }else if(startNum > finishNum){
        var currentNum = startNum
        for(var i = 0; currentNum >= finishNum;i++){
            rangearray.push(currentNum)
            currentNum -= step
        }
    }else if(!startNum || !finishNum || step){
        return -1
    }
    return rangearray
}
console.log('rangeWithStep(1, 10, 2):', rangeWithStep(1, 10, 2))
console.log('rangeWithStep(11, 23, 3):', rangeWithStep(11, 23, 3))
console.log('rangeWithStep(5, 2, 1):', rangeWithStep(5, 2, 1))
console.log('rangeWithStep(29, 2, 4):', rangeWithStep(29, 2, 4))
console.log('---------- 3 ----------')
function sum(a, Un, b){
    var rangearray = []
    var distance
    if(!b){
        distance = 1
    }else{
        distance = b
    }
    if(a < Un){
        var currentNum = a
        for(var i = 0;currentNum <= Un; i++){
            rangearray.push(currentNum)
            currentNum += distance
        }
    }else if(a > Un){
        var currentNum = a
        for(var i = 0; currentNum >= Un; i++){
            rangearray.push(currentNum)
            currentNum -= distance
        }
    }else if(!a && !Un && !b){
        return 0
    }else if(a){
        return a
    }
    var Sn = 0
    for(var i = 0; i< rangearray.length; i++){
        Sn = Sn + rangearray[i]
    }
    return Sn
}
console.log('sum(1,10):', sum(1,10))
console.log('sum(5, 50, 2):', sum(5, 50, 2)) 
console.log('sum(15,10):', sum(15,10))
console.log('sum(20, 10, 2):', sum(20, 10, 2)) 
console.log('sum(1):', sum(1)) 
console.log('sum():', sum()) 
console.log('---------- 4 ----------')
function dataHandling(data){
    var datalength = data.length
    for(var i = 0; i < datalength; i++){
        var no = 'Nomor ID: ' + data[i][0]
        var  nama = 'Nama Lengkap: ' + data[i][1]
        var ttl = 'TTL: ' + data[i][2] +' '+ data[i][3]
        var hobi = 'Hobi: ' + data[i][4]
        console.log(no)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
    }
}
var input = [["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]] 
dataHandling(input)
console.log('---------- 5 ----------')
function balikKata(sentence){
    var word = ' '
    for(var i = sentence.length-1;i>=0;i--){
        word += sentence[i]
    }
    return word
}
console.log('balikKata("Kasur Rusak"):', balikKata("Kasur Rusak"))
console.log('balikKata("SanberCode"):', balikKata("SanberCode"))
console.log('balikKata("Haji Ijah"):', balikKata("Haji Ijah"))
console.log('balikKata("racecar"):', balikKata("racecar"))
console.log('balikKata("I am Sanbers"):', balikKata("I am Sanbers"))
console.log('---------- 6 ----------')
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]
function dataHandling2(data){
    var data1 = data
    var nama = data[1] + ' Elsharawy'
    var prov = 'Provinsi ' + data[2]
    var gender = 'Pria'
    var sekolah = 'SMA Internasional Metro'
    data1.splice(1, 1, nama)
    data1.splice(2, 1, prov)
    data1.splice(4, 1, gender, sekolah)
    var tanggal = data[3]
    var tanggal1 = tanggal.split('/')
    var bulan = tanggal1[1]
    var namabulan = ' '
    switch(bulan){
        case '01':
            namabulan = 'Januari'
            break
        case '02':
            namabulan = 'Februari'
            break
        case '03':
            namabulan = 'Maret'
            break
        case '04':
            namabulan = 'April'
            break
        case '05':
            namabulan = 'Mei'
            break
        case '06':
            namabulan = 'Juni'
            break
        case '07':
            namabulan = 'Juli'
            break
        case '08':
            namabulan = 'Agustus'
            break
        case '09':
            namabulan = 'September'
        case '10':
            namabulan = 'Oktober'
            break
        case '11':
            namabulan = 'November'
            break
        case '12':
            namabulan = 'Desember'
            break
        default:
            break
    }
    var date = tanggal1.join('-')
    var datearray = tanggal1.sort(function(a,b){
        b - a
    })
    var newname = nama.slice(0,15)
    console.log(data1)
    console.log(namabulan)
    console.log(datearray)
    console.log(date)
    console.log(newname)
}
dataHandling2(input)